#!/usr/bin/perl

  open(MASK,"mask");
  open(IN,">cp2k.in"); 
  open(SYSTEM,"system");
  open(COORD,"coord");
  while(<MASK>)
  {
    if(/#####/)
    {
      $n=0;
      while(<SYSTEM>=~/\s*(\S+)\s*(\S+)\s*/)
      {
        $n=  $n+1;
        $sy= $1;
        <COORD>=~/\s*(\S+)\s*(\S+)\s*(\S+)/;
        printf(IN"%2s %22.20f %22.20f %22.20f\n",$sy,$1,$2,$3);
      }
    }
    else
    {
      print(IN);
    }
  }
  close(MASK);
  close(IN);
  close(COORD);
  close(SYSTEM);

  open(MO,"| mpirun -np 20 ./cp2k.popt -r -i cp2k.in > cp2k.out ");
  close(MO);

  open(GRAD,">grad");

  open(OUT,"cp2k.out");
  while($x=<OUT>)
  {
    if($x=~/ENERGY/ && $x=~/Total/)
    {
      $x=~/(\S+)\n/;
      printf(GRAD"%s\n",$1);
      for($i=0; $i<5; $i++)
      {
        $x=<OUT>;
      }
      for($i=0; $i<$n; $i++)
      {
        $x=<OUT>;
        $x=~/\s*\S+\s*\S+\s*\S+\s*(\S+)\s*(\S+)\s*(\S+)\n/;
#        printf(GRAD"%s %s %s\n",-$1,-$2,-$3);    ## I remove this negative sign
        printf(GRAD"%s %s %s\n",$1,$2,$3);
      }
    }
  }
  close(OUT);

  close(GRAD);