# MD driver with PCA force

This code perform MD simulation with driving force from principal component analysis

md_driver.py will do the following:
1. Read 6 controlling parameters from "in-md-pmf": Time step, Total steps, Number of data printing steps, Temperature, Number of thermo control steps, PCA force rate
2. Read molecular system from "system" (Atoms and atomic masses) and coordinate from "coor0"
3. Assign randomized initial velocity using Boltzmann distribution
4. Perform principal component analysis (using PathReducer, Hare et al., Chem. Sci. 10, 9954, 2019).
5. Calculate eigenvector for PC projection, and employ it to construct PCA forces.
6. Integrate equations of motion with total force = DFT force + PCA forces.
  i. Create CP2K using file "mask" as a template
  ii. Run CP2K and extract force data with the script bb