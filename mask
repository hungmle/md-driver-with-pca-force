@SET BASE_NAME Oh_MM
@SET ID 1
@SET XYZ_FILE_NAME new.xyz
@SET BOX_LENGTH 14.3
@SET ENSEMBLE_TYPE NVT

&GLOBAL
    PROJECT Oh_MM
    PREFERRED_FFT_LIBRARY FFTW
    RUN_TYPE ENERGY_FORCE
    PRINT_LEVEL MEDIUM
    WALLTIME 01:00:00 
&END GLOBAL

#&EXT_RESTART
#    RESTART_FILE_NAME aloh4_gopt.restart 
#    RESTART_DEFAULT T
#    RESTART_POS T
#    RESTART_CONSTRAINT T
#    RESTART_METADYNAMICS F
#&END EXT_RESTART

&FORCE_EVAL
    STRESS_TENSOR ANALYTICAL
    METHOD FIST
    &MM
      &PRINT
        &FF_INFO HIGH
        &END FF_INFO
      &END PRINT
      &FORCEFIELD
        PARMTYPE OFF               #Force field through the input file
        &SPLINE
#          EMAX_SPLINE 10
#          R0_NB 0.2
        &END SPLINE
        &CHARGE
          ATOM Al
          CHARGE 1.5750
        &END CHARGE
        &CHARGE
          ATOM Ok
          CHARGE -1.05875
        &END CHARGE
        &CHARGE
          ATOM Hk
          CHARGE 0.415 
        &END CHARGE
        &CHARGE
          ATOM O
          CHARGE -0.8200
        &END CHARGE
        &CHARGE
          ATOM H
          CHARGE 0.4100
        &END CHARGE
        &BOND
          ATOMS O H
          KIND CHARMM 
          K [angstrom^-2kcalmol] 554.13       
          R0 [angstrom] 1.0
        &END BOND
        &BOND
          ATOMS Ok Hk
          KIND MORSE
          K [kcalmol] 132.2491 [angstrom^-1] 2.1350   
          R0 [angstrom] 0.945
        &END BOND
        &BEND
          ATOMS H O H
          KIND CHARMM
          K [rad^-2kcalmol] 45.770          
          THETA0 [deg] 109.47
        &END BEND
#        &BEND
#          ATOMS Al Ok Hk
#          KIND CHARMM
#          K [rad^-2kcalmol] 15
#          THETA0 [deg] 110
#        &END BEND
        
        &NONBONDED

#       Leonard Jones potential parameters
#       Al     0.0000013298    4.2712360000
#       H      0.0000000000    0.0000000000
#       Hk     0.0000000000    0.0000000000
#       O      0.1553999393    3.1655414285
#       Ok     0.1553999393    3.1655414285
#       epsiloij=sqrt(ei*ej) sigmaij=1/2(sigi+sigj)
          
          &LENNARD-JONES
            ATOMS Al Al
            EPSILON [kcalmol] 0.0000013298
            SIGMA 4.2718
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Al Ok
            EPSILON [kcalmol] 0.0004545887372
            SIGMA 3.71865
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Al Hk
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Al O
            EPSILON [kcalmol] 0.0004545887372
            SIGMA 3.71865
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Al H
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS O O
            EPSILON [kcalmol] 0.155399939
            SIGMA 3.165541429
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS O Ok
            EPSILON [kcalmol] 0.155399939
            SIGMA 3.165541429
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Ok Ok
            EPSILON [kcalmol] 0.155399939
            SIGMA 3.165541429
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS O Hk
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS O H
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Ok H
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Ok Hk
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS H H
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS Hk Hk
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES
          &LENNARD-JONES
            ATOMS H Hk
            EPSILON [kcalmol] 0
            SIGMA 0
            RCUT 12
          &END LENNARD-JONES

        &END NONBONDED

      &END FORCEFIELD
      &POISSON
        &EWALD
          EWALD_TYPE SPME
          GMAX 20
        &END EWALD
      &END POISSON
    &END MM

    &SUBSYS

        &CELL
            ABC [angstrom] ${BOX_LENGTH} ${BOX_LENGTH} ${BOX_LENGTH} 
            PERIODIC XYZ
        &END CELL

	&COORD
#####
	&END COORD

        &TOPOLOGY
            COORD_FILE_NAME ./${XYZ_FILE_NAME}
            COORD_FILE_FORMAT XYZ
            &CENTER_COORDINATES T
            &END CENTER_COORDINATES 
	    &GENERATE
              BONDLENGTH_MAX 1.2
              CREATE_MOLECULES TRUE
#             &ANGLE ADD
#		ATOMS 274 275 276
#	      &END ANGLE
#	      &ANGLE ADD
#		ATOMS 274 277 278
#	      &END ANGLE
#	      &ANGLE ADD
#		ATOMS 274 279 280
#	      &END ANGLE
#	      &ANGLE ADD
#		ATOMS 274 281 282
#	      &END ANGLE
	      &NEIGHBOR_LISTS
		GEO_CHECK TRUE
	      &END NEIGHBOR_LISTS
	    &END GENERATE
        &END TOPOLOGY
        &KIND H
	    ELEMENT H
        &END KIND
        &KIND O
	    ELEMENT O
        &END KIND
        &KIND Hk
	    ELEMENT H
        &END KIND
        &KIND Ok
	    ELEMENT O
        &END KIND        
#        &KIND Na
#        &END KIND
        &KIND Al
        &END KIND

    &END SUBSYS

    &PRINT
        &FORCES ON
        &END FORCES
    &END PRINT

&END FORCE_EVAL

